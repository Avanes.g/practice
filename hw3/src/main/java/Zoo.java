import animals.behavior.Swim;
import animals.carnivorous.Carnivorous;
import animals.carnivorous.Fish;
import animals.carnivorous.Lion;
import animals.carnivorous.Panther;
import animals.herbivore.Deer;
import animals.herbivore.Duck;
import animals.herbivore.Herbivore;
import animals.herbivore.Zebra;
import aviary.Aviary;
import exceptions.WrongFoodException;
import food.Grass;
import food.Meat;
import workers.Worker;

public class Zoo {
    public static void main(String[] args) {


        //Хищные животные
        Fish fish = new Fish("Игорь", 2);
        Lion lion = new Lion("Валера", 3);
        Panther panther = new Panther("Женя", 6);

        //Травоядные животные
        Deer deer = new Deer("Павел", 20);
        Duck duck = new Duck("Дональд", 40);
        Zebra zebra = new Zebra("Семен", 33);

        //Еда
        Grass grass = new Grass();
        Meat meat = new Meat();

        Worker worker = new Worker();
        try {
            worker.feed(fish, meat);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
        try {
            worker.feed(fish, grass);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }


        try {
            worker.feed(lion, grass);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
        try {
            worker.feed(lion, meat);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        try {
            worker.feed(panther, grass);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
        try {
            worker.feed(panther, meat);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        try {
            worker.feed(deer, grass);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
        try {
            worker.feed(deer, meat);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        try {
            worker.feed(duck, grass);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
        try {
            worker.feed(duck, meat);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        try {
            worker.feed(zebra, grass);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
        try {
            worker.feed(zebra, meat);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        System.out.printf("Пиранья сыта на  %s%n", fish.getSatietyScale());
        System.out.printf("Лев сыт на  %s%n", lion.getSatietyScale());
        System.out.printf("Пантера сыта на  %s%n", panther.getSatietyScale());
        System.out.printf("Олень сыт на  %s%n", deer.getSatietyScale());
        System.out.printf("Утка сыта на  %s%n", duck.getSatietyScale());
        System.out.printf("Зебра сыта на  %s%n", zebra.getSatietyScale());

        //Ошибка на уровне компиляции так как у рыбы нету голоса
        //System.out.println(worker.getVoice(fish));
        System.out.println(worker.getVoice(lion));
        System.out.println(worker.getVoice(panther));
        System.out.println(worker.getVoice(deer));
        System.out.println(worker.getVoice(duck));
        System.out.println(worker.getVoice(zebra));

        Aviary<Carnivorous> aviaryCarnivorous = new Aviary<>(Aviary.Size.BIG);
        aviaryCarnivorous.addAnimal(lion);
        aviaryCarnivorous.addAnimal(panther);
        aviaryCarnivorous.removeAnimal(lion);
        System.out.println(aviaryCarnivorous.getAnimal("Женя").getClass());


        Aviary<Herbivore> aviaryHerbivore = new Aviary<>(Aviary.Size.BIG_MAX);
        aviaryHerbivore.addAnimal(zebra);
        aviaryHerbivore.addAnimal(deer);
        aviaryHerbivore.addAnimal(duck);
        aviaryHerbivore.removeAnimal(zebra);
        System.out.println(aviaryHerbivore.getAnimal("Павел"));

        //Пруд
        Swim[] pond = {
                new Duck("Берк", 12),
                new Duck("Катя", 12),
                new Fish("Семми", 12),
                new Fish("Кент", 12),
                new Fish("Буч", 12)
        };
        for (Swim waterfowl : pond) {
            waterfowl.swim();
        }
    }
}