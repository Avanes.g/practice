package workers;

import animals.Animal;
import animals.behavior.Voice;
import exceptions.WrongFoodException;
import food.Food;

public class Worker {
    public void feed(Animal animal, Food food) throws WrongFoodException {
        animal.eat(food,animal);
    }

    public String getVoice(Voice voice) {
        return voice.voice();
    }
}
