package animals.carnivorous;

import animals.behavior.Swim;
import aviary.Aviary;

public class Fish extends Carnivorous implements Swim {
    public Fish(String nickname, int age) {
        super(nickname, age);
        setAviarySize(Aviary.Size.SMALL);
    }

    public Fish(String nickname, int age, int satietyScale) {
        super(nickname, age);
        setAviarySize(Aviary.Size.SMALL);
    }

    @Override
    public void swim() {
        System.out.printf("Рыба %s поплыла%n", getNickname());
        setSatietyScale(getSatietyScale() - 1);
    }

}