package animals.carnivorous;

import animals.behavior.Run;
import animals.behavior.Voice;
import aviary.Aviary;

public class Panther extends Carnivorous implements Run, Voice {
    public Panther(String nickname, int age) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG);
    }

    public Panther(String nickname, int age, int satietyScale) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG);
    }

    @Override
    public void run() {
        System.out.println("Пантера бежит");
        setSatietyScale(getSatietyScale() - 1);
    }

    @Override
    public String voice() {
        return "Пантера рычит";
    }

}