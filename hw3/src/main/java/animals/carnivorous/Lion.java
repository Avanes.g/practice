package animals.carnivorous;

import animals.behavior.Run;
import animals.behavior.Voice;
import aviary.Aviary;

public class Lion extends Carnivorous implements Run, Voice {
    public Lion(String nickname, int age) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG);
    }

    public Lion(String nickname, int age, int satietyScale) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG);
    }

    @Override
    public void run() {
        System.out.println("Лев побежал");
        setSatietyScale(getSatietyScale() - 2);
    }

    @Override
    public String voice() {
        return "Лев рычит";
    }

}
