package animals.carnivorous;

import animals.Animal;
import exceptions.WrongFoodException;
import food.Food;

public abstract class Carnivorous extends Animal {
    public Carnivorous(String nickname, int age) {
        super(nickname, age);
    }

    public Carnivorous(String nickname, int age, int satietyScale) {
        super(nickname, age, satietyScale);
    }

}



