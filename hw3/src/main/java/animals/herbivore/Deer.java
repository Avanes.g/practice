package animals.herbivore;

import animals.behavior.Run;
import animals.behavior.Voice;
import aviary.Aviary;

public class Deer extends Herbivore implements Run, Voice {
    public Deer(String nickname, int age) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG_MAX);
    }

    public Deer(String nickname, int age, int satietyScale) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG_MAX);
    }

    @Override
    public String voice() {
        return "Олень ревет";
    }

    @Override
    public void run() {
        System.out.println("Олень пошел");
        setSatietyScale(getSatietyScale() - 3);

    }

}