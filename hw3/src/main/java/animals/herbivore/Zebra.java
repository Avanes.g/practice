package animals.herbivore;
import animals.behavior.Run;
import animals.behavior.Voice;
import aviary.Aviary;

public class Zebra extends Herbivore implements Run, Voice {

    public Zebra(String nickname, int age) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG_MAX);
    }

    public Zebra(String nickname, int age, int satietyScale) {
        super(nickname, age);
        setAviarySize(Aviary.Size.BIG_MAX);
    }

    @Override
    public String voice() {
        return "Зебра издает странные звуки";
    }

    @Override
    public void run() {
        System.out.println("Зебра побежала");
        setSatietyScale(getSatietyScale() - 1);
    }
}
