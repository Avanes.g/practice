package animals;

import animals.carnivorous.Carnivorous;
import animals.herbivore.Herbivore;
import aviary.Aviary;
import exceptions.WrongFoodException;
import food.Food;
import food.Grass;
import food.Meat;

public abstract class Animal {
    private String nickname;
    private int age;
    private int satietyScale = 50;
    private Aviary.Size aviarySize;


    public Animal(String nickname, int age) {
        this.nickname = nickname;
        this.age = age;
    }

    public Animal(String nickname, int age, int satietyScale) {
        this.nickname = nickname;
        this.age = age;
        this.satietyScale = satietyScale;
    }

    public Aviary.Size getAviarySize() {
        return aviarySize;
    }

    public void setAviarySize(Aviary.Size aviarySize) {
        this.aviarySize = aviarySize;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSatietyScale() {
        return satietyScale;
    }

    public void setSatietyScale(int satietyScale) {
        this.satietyScale = satietyScale;
    }

    public void eat(Food food, Animal animal) throws WrongFoodException {
        checkToEat(food, getNickname(), animal);
        setSatietyScale(getSatietyScale() + food.getSatietyOfFood());
    }

    public void checkToEat(Food food, String name, Animal animal) throws WrongFoodException {
        if ((!(food instanceof Meat) && !(animal instanceof Herbivore)) ||
                (!(food instanceof Grass) && !(animal instanceof Carnivorous))) {
            throw new WrongFoodException(String.format("%s я такое не ем", name));
        }
        System.out.printf("%s с удовольсвие ест %n", name);

    }

    @Override
    public int hashCode() {
        return getNickname().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ((Animal) obj).getNickname().equals(getNickname());

    }

}