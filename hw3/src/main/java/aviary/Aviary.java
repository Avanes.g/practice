package aviary;

import animals.Animal;

import java.util.HashMap;
import java.util.Map;

public class Aviary<T extends Animal> {
    public enum Size {
        SMALL(1), MEDIUM(2), BIG(3), BIG_MAX(4);
        int index = 0;

        Size(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }

    private final Size size;

    private final Map<String, T> aviaryResidents;

    public Aviary(Size size) {
        this.size = size;
        aviaryResidents = new HashMap<>();

    }

    public void addAnimal(T animal) {
        if (animal.getAviarySize().index > size.index) {
            System.out.printf("Вальер не подходит для %s", animal.getNickname());
            System.out.println();
            return;
        }
        aviaryResidents.put(animal.getNickname(), animal);

    }

    public void removeAnimal(T animal) {
        removeAnimal(animal.getNickname());
    }

    public void removeAnimal(String nickname) {
        aviaryResidents.remove(nickname);

    }

    public T getAnimal(String nickName) {
        return aviaryResidents.get(nickName);
    }
}
