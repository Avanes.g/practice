package model;

public class Kotik {

    private String name;
    private int prettiness;
    private int weight;
    private String meow;
    private int countHunger = 0;
    private static int countCat = 0;

    public Kotik() {
        countCat++;
    }

    public Kotik(String name, int prettiness, int weight, String meow) {
        this.name = name;
        this.prettiness = prettiness;
        this.weight = weight;
        this.meow = meow;
        countCat++;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }


    public String getMeow() {
        return meow;
    }

    public static int countCat() {
        return countCat++;
    }

    public static int getCountCat() {
        return countCat;
    }

    private boolean checkHunger() {
        return (countHunger <= 0);
    }


    private boolean play() {
        if (!checkHunger()) {
            System.out.println("I play");
            countHunger--;
            return true;
        }  return false;
    }

    private boolean sleep() {
        if (!checkHunger()) {
            System.out.println("i sleep");
            countHunger--;
            return true;
        }  return false;
    }

    private boolean chaseMouse() {
        if (!checkHunger()) {
            System.out.println("I am catching a mouse");
            countHunger--;
            return true;
        }  return false;
    }

    private boolean eat(int satiety) {
        System.out.println("I am eating");
        countHunger++;
        return true;
    }

    private boolean eat(int satiety, String nameFood) {
        if (satiety > 0) {
            return false;
        } else {
            System.out.println("I am eating");
            countHunger++;
            return true;
        }
    }


    private boolean eat() {
        return eat(countHunger, "Wiscas");
    }


    private boolean purring() {
        if (!checkHunger()) {
            System.out.println("I am catching a mouse");
            countHunger--;
            return true;
        }
        return false;
    }

    private boolean randomMethod() {
        int random = (int) (Math.random() * 4) + 1;
        boolean result = false;
        switch (random) {

            case 1:
                result = play();
                break;
            case 2:
                result = sleep();
                break;
            case 3:
                result = chaseMouse();
                break;
            case 4:
                result = purring();
                break;
        }
        return result;
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            if (!randomMethod()) {
                eat();
            }

        }
    }
}
