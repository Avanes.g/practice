import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik barsik = new Kotik();
        Kotik sara = new Kotik("Sara", 5, 5, "may");
        barsik.setName("barsik");
        barsik.setPrettiness(2);
        barsik.setWeight(8);
        barsik.setMeow("MAyyyy");
        barsik.liveAnotherDay();
        System.out.println("Результат сравнения мяуканья : " + barsik.getMeow().equals(sara.getMeow()));
        System.out.println("Колличесво созданных котов : " + Kotik.getCountCat());
    }
}
