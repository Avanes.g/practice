package animals.herbivore;

import animals.Animal;

public abstract class Herbivore extends Animal {
    public Herbivore(String nickname, int age) {
        super(nickname, age);
    }

    public Herbivore(String nickname, int age, int satietyScale) {
        super(nickname, age, satietyScale);
    }

}
