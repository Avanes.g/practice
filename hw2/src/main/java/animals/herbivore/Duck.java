package animals.herbivore;

import animals.behavior.Fly;
import animals.behavior.Run;
import animals.behavior.Swim;
import animals.behavior.Voice;

public class Duck extends Herbivore implements Fly, Swim, Run, Voice {
    public Duck(String nickname, int age) {
        super(nickname, age);
    }

    public Duck(String nickname, int age, int satietyScale) {
        super(nickname, age);
    }

    @Override
    public void fly() {
        System.out.println("Утра полетела");
    }

    @Override
    public void run() {
        System.out.printf("Утка %s побежала%n", getNickname());
        setSatietyScale(getSatietyScale() - 2);
    }

    @Override
    public void swim() {
        System.out.printf("Утка %s поплыла%n", getNickname());
        setSatietyScale(getSatietyScale() - 1);
    }

    @Override
    public String voice() {
        return "Утка крякает";
    }

}
