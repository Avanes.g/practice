package animals.herbivore;

import animals.behavior.Run;
import animals.behavior.Voice;

public class Zebra extends Herbivore implements Run, Voice {

    public Zebra(String nickname, int age) {
        super(nickname, age);
    }

    public Zebra(String nickname, int age, int satietyScale) {
        super(nickname, age);
    }

    @Override
    public String voice() {
        return "Зебра издает странные звуки";
    }

    @Override
    public void run() {
        System.out.println("Зебра побежала");
        setSatietyScale(getSatietyScale() - 1);

    }
}
