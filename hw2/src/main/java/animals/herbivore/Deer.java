package animals.herbivore;

import animals.behavior.Run;
import animals.behavior.Voice;

public class Deer extends Herbivore implements Run, Voice {
    public Deer(String nickname, int age) {
        super(nickname, age);
    }

    public Deer(String nickname, int age, int satietyScale) {
        super(nickname, age);
    }

    @Override
    public String voice() {
        return "Олень ревет";
    }

    @Override
    public void run() {
        System.out.println("Олень пошел");
        setSatietyScale(getSatietyScale() - 3);
    }
}