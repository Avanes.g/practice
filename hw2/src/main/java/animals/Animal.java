package animals;

import animals.carnivorous.Carnivorous;
import animals.herbivore.Herbivore;
import food.Food;
import food.Grass;
import food.Meat;

public abstract class Animal {
    private String nickname;
    private int age;
    private int satietyScale = 50;


    public Animal(String nickname, int age) {
        this.nickname = nickname;
        this.age = age;
    }

    public Animal(String nickname, int age, int satietyScale) {
        this.nickname = nickname;
        this.age = age;
        this.satietyScale = satietyScale;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSatietyScale() {
        return satietyScale;
    }

    public void setSatietyScale(int satietyScale) {
        this.satietyScale = satietyScale;
    }

    public void eat(Food food, Animal animal) {
        if (checkToEat(food, getNickname(), animal))
            setSatietyScale(getSatietyScale() + food.getSatietyOfFood());
    }

    public boolean checkToEat(Food food, String name, Animal animal) {
        if ((!(food instanceof Meat) && !(animal instanceof Herbivore)) ||
                (!(food instanceof Grass) && !(animal instanceof Carnivorous))) {

            System.out.printf("%s я такое не ем%n", name);
            return false;
        }

        System.out.printf("%s с удовольсвие ест %n", name);
        return true;
    }


}