package animals.behavior;

public interface Voice {
    String voice();
}
