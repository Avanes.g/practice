package animals.carnivorous;

import animals.behavior.Swim;

public class Fish extends Carnivorous implements Swim {
    public Fish(String nickname, int age) {
        super(nickname, age);
    }

    public Fish(String nickname, int age, int satietyScale) {
        super(nickname, age);
    }

    @Override
    public void swim() {
        System.out.printf("Рыба %s поплыла%n", getNickname());
        setSatietyScale(getSatietyScale() - 1);
    }
}