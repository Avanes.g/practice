package animals.carnivorous;

import animals.Animal;

public abstract class Carnivorous extends Animal {
    public Carnivorous(String nickname, int age) {
        super(nickname, age);
    }

    public Carnivorous(String nickname, int age, int satietyScale) {
        super(nickname, age, satietyScale);
    }

}

