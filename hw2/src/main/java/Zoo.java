import animals.behavior.Swim;
import animals.carnivorous.Fish;
import animals.carnivorous.Lion;
import animals.carnivorous.Panther;
import animals.herbivore.Deer;
import animals.herbivore.Duck;
import animals.herbivore.Zebra;
import food.Grass;
import food.Meat;
import workers.Worker;

public class Zoo {
    public static void main(String[] args) {


        //Хищные животные
        Fish fish = new Fish("Игорь", 2);
        Lion lion = new Lion("Валера", 3);
        Panther panther = new Panther("Женя", 6);

        //Травоядные животные
        Deer deer = new Deer("Павел", 20);
        Duck duck = new Duck("Дональд", 40);
        Zebra zebra = new Zebra("Семен", 33);

        //Еда
        Grass grass = new Grass();
        Meat meat = new Meat();

        Worker worker = new Worker();
        worker.feed(fish, grass);
        worker.feed(fish, meat);

        worker.feed(lion, grass);
        worker.feed(lion, meat);

        worker.feed(panther, grass);
        worker.feed(panther, meat);

        worker.feed(deer, grass);
        worker.feed(deer, meat);

        worker.feed(duck, grass);
        worker.feed(duck, meat);

        worker.feed(zebra, grass);
        worker.feed(zebra, meat);

        System.out.printf("Пиранья сыта на  %s%n", fish.getSatietyScale());
        System.out.printf("Лев сыт на  %s%n", lion.getSatietyScale());
        System.out.printf("Пантера сыта на  %s%n", panther.getSatietyScale());
        System.out.printf("Олень сыт на  %s%n", deer.getSatietyScale());
        System.out.printf("Утка сыта на  %s%n", duck.getSatietyScale());
        System.out.printf("Зебра сыта на  %s%n", zebra.getSatietyScale());

        //Ошибка на уровне компиляции так как у рыбы нету голоса
        //System.out.println(worker.getVoice(fish));
        System.out.println(worker.getVoice(lion));
        System.out.println(worker.getVoice(panther));
        System.out.println(worker.getVoice(deer));
        System.out.println(worker.getVoice(duck));
        System.out.println(worker.getVoice(zebra));


        //Пруд
        Swim[] pond = {
                new Duck("Берк", 12),
                new Duck("Катя", 12),
                new Fish("Семми", 12),
                new Fish("Кент", 12),
                new Fish("Буч", 12)
        };
        for (Swim waterfowl : pond) {
            waterfowl.swim();
        }
    }
}