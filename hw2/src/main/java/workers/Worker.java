package workers;

import animals.Animal;
import animals.behavior.Voice;
import food.Food;

public class Worker {
    public void feed(Animal animal, Food food) {
        animal.eat(food, animal);
    }

    public String getVoice(Voice voice) {
        return voice.voice();
    }
}
